> 欢迎浏览我的博客 获取更多精彩文章
>
> https://boyn.top

# Spring的数据库事务管理

数据库的事务管理一直是一个难点,在如今并发量越来越大的情况下,数据库在多事务访问的环境中容易引发数据丢失和一些数据一致性的问题.而事务管理就是为了解决这些问题的.在大部分情况下,我们要求一系列的数据库操作是保持原子性的,即要么全部成功,要么全部失败,但是也会有其他的要求和需要.在Spring中,事务的传播行为就可以很好地满足我们不停变化的需要.

## Spring的事务管理设计

在Spring的应用中,有着编程式事务和声明式事务两种管理方式

TransactionTemplate是Spring提供的事务管理器的模板,它提供的是编程式的事务管理,他的执行逻辑如下:

- 事务中的创建,提交和回滚都是通过PlatformTransactionManager这个接口来完成的.
- 当事务产生异常的时候会发生回滚
- 无异常,就会提交事务

在Spring中,编程式的事务管理已经很少用到了,目前主流的方式是使用@Transactional注解来进行声明式事务的实现.

首先,我们需要声明一个TransactionManager的Bean

```java
@Bean("transactionManager")
	public PlatformTransactionManager annotationDrivenTransactionManager(){
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(dataSource);
		return transactionManager;
	}
```

此处是在一个配置类里面声明的,还需要再加上一个@EnableTransactionManagement的类注解

这样Spring就会知道要使用这个事务管理器来管理事务了

## @Transactional注解

这个注解是开启Spring事务管理的关键,我们先来看一下他的接口配置项

![](http://imageblog.boyn.top/201908180852_321.png)

![](http://imageblog.boyn.top/201908180852_27.png)

在其中,最重要的就是isolation和propagation

其中牵涉到了数据库的事务特性,会在稍后讲述

有了上面的这些东西,我们就可以在Spring中对数据库事务进行管理了.不过首先,我们需要知道,声明式事务的流程是怎么样的

![](http://imageblog.boyn.top/201908180915_736.png)

Spring通过PlatformTransactionManager的子类作为事务管理器来创建事务,与此同时,会将我们设置的隔离级别等属性往事务上面配置.我们只需要完成配置,而无需指定运行的方式.

## 数据库事务的特性

在数据库执行的事务过程中,最重要的是4个基本要素

- 原子性:一个事务中的所有操作,要么全部成功完成并提交,要么有任意一个失败就会回滚
- 一致性:一个事务可以改变封装的状态,不管在任意时刻的并发量有多少,其系统都会处于一致
- 隔离性:两个事务之间的隔离程度
- 持久性:在事务完成并提交后,事务的更改便被一直保存在数据库中,不会回滚



其中,隔离性的级别又被分为4层,分别是:

- 脏读(Dirty Read):允许一个事务去读取另外一个事务中未提交的数据,这个方法由于相当于事务之间不产生隔离,所以效率很高,但是出错的概率也很大,当一个事务读取了另外一个事务未提交的数据后,根据这个数据来进行下面的操作,但是另一个事务如果发生了回滚,那么所有事务都会出错
- 读写提交(Read Commit):这个隔离级别是指,当一个事务未提交时,另外的事务不能读取到它修改的数据.
- 可重复读(Repeatable Read):可重复读会使得同一条数据库的记录的读写按照一个序列化来进行操作,不会产生交叉的情况,这样就可以保证一条数据的一致性,但是会出现幻读
- 序列化:让SQL按照顺序进行读写

![](http://imageblog.boyn.top/201908180928_85.png)

一般来说,从脏读到序列化,性能是一路下降的,在大部分时间,我们都可以选择读写提交的隔离级别来完成大部分的操作,但是也会在一定情况下带来数据的一致性问题

同时,隔离性的不同,也会带来传播行为的不同.当一个事务发生了失败的时候,跟这一个事务有关的其他事务要根据什么逻辑去处理这个回滚,这就是传播行为的策略.在Spring中,一共有7种传播行为

![](http://imageblog.boyn.top/201908181007_66.png)