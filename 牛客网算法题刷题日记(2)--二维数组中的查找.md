# 牛客网算法题刷题日记(2)--二维数组中的查找

| 解决问题                                                     | 提交时间   | 状态     | 运行时间 | 占用内存 | 使用语言 |
| ------------------------------------------------------------ | ---------- | -------- | -------- | -------- | -------- |
| [二维数组中的查找](https://www.nowcoder.com/profile/3143587/codeBookDetail?submissionId=45221672) | 2019-04-10 | 答案正确 | 160 ms   | 16756K   | Java     |

## 问题链接:

<https://www.nowcoder.com/questionTerminal/abc3fe2ce8e146608e868a70efebf62e?orderByHotValue=1&page=1&onlyReference=false>

## 问题思路:

一个十分容易想到的办法就是:

将数组的每一行列的最值跟目标值对比,如果在其中,则使用二分查找法进行查找

代码如下:

```java
import java.util.Arrays;
/*
    Project: Niuke
    Author: Boyn
    Date: 2019/4/10
*/
public class TwoDimensionSearch {
    public boolean Find(int target, int [][] array) {
        int colLength = array[0].length;
        int rowLength = array.length;//获取行列的长度
        if(colLength == 0) return false;//如果数组为空,直接返回false
        int[] rowMin = array[0];
        int[] rowMax = array[rowLength-1];
        int[] colMin = new int[rowLength];
        int[] colMax = new int[rowLength];//获取行列的最大值
        for(int i = 0;i<rowLength;i++){
            colMin[i] = array[i][0];
            colMax[i] = array[i][colLength-1];
        }
        for(int col = 0;col<colLength;col++){
            int maxPerRow = rowMax[col];
            int minPerRow = rowMin[col];
            if(target<=maxPerRow&&target>=minPerRow){//跟行比较
                for(int row=0;row<rowLength;row++){
                    int maxPerCol = colMax[row];
                    int minPerCol = colMin[row];
                    if(target<=maxPerCol&&target>=minPerCol){//跟列比较

                        int[] colSearch = new int[rowLength];
                        for(int i = 0;i<rowLength;i++){
                            colSearch[i] = array[i][col];
                        }
                        int flag=-1;
                        flag = Arrays.binarySearch(colSearch,target);//进行二叉搜索
                        if(flag>=0) return true;
                    }
                }
            }
        }
        return false;

    }
}
```

但是,在我提交了之后,看到了另外一种思路:

根据这个数组的特性,其左下角值是列中最大,行中最小的,所以可以从左下角开始进行搜索,当目标值小,则向右移动,当目标值大,则向左移动

```java
链接：https://www.nowcoder.com/questionTerminal/abc3fe2ce8e146608e868a70efebf62e?orderByHotValue=1&page=1&onlyReference=false
来源：牛客网

class Solution {
public:
    bool Find(vector<vector<int> > array,int target) {
        int rowCount = array.size();
        int colCount = array[0].size();
        int i,j;
        for(i=rowCount-1,j=0;i>=0&&j<colCount;)
        {
            if(target == array[i][j])
                return true;
            if(target < array[i][j])
            {
                i--;
                continue;
            }
            if(target > array[i][j])
            {
                j++;
                continue;
            }
        }
        return false;
    }
};

```

## 反思:

其实这个问题很简单,思路也很清晰,但是却花了1个多小时,其中一部分原因是对Arrays类里面的binarySearch搜索不了解,其官方文档如下,其中,最重要的就是他的返回值解释

#### binarySearch

```java
public static int binarySearch(int[] a,int key)
```

Searches the specified array of ints for the specified value using the binary search algorithm. The array must be sorted (as by the [`sort(int[\])`]prior to making this call. If it is not sorted, the results are undefined. If the array contains multiple elements with the specified value, there is no guarantee which one will be found.

- **Parameters:**

  `a` - the array to be searched

  `key` - the value to be searched for

- **Returns:**

  index of the search key, if it is contained in the array; otherwise, `(-(*insertion point*) - 1)`. The *insertion point* is defined as the point at which the key would be inserted into the array: the index of the first element greater than the key, or `a.length` if all elements in the array are less than the specified key. Note that this guarantees that the return value will be >= 0 if and only if the key is found.

  这个方法返回的是目标值的索引,如果没有找到这个值,就会返回一个关于插入点的函数,这个插入点定义为第一个大于这个值的元素, 其索引为index = (-插入点-1)

  如果找到了这个元素,他就会返回>=0的值,如果我们只需要知道是否在数组内的话,只需要判断返回的int是否非零就行了.