> 欢迎浏览我的博客 获取更多精彩文章
>
> https://boyn.top

# AspectJ的基本使用

在Java的应用开发中,我们经常会遇到要使用面向切面编程的情况,而AspectJ就是一个很好的AOP库.

## 什么是面向切面编程?

面向切面编程是根据应用场景来进行命名的.在实际应用中,有的事情不是仅仅通过面向对象的编程就可以解决的,比如在数据库的事务处理,我们在一次交易中,可能需要查询多次数据库,并且确保这些操作都是具有原子性的,即要么全部成功,要么全部失败.所以,这个时候面向对象编程就可以派上用场了



我们可以总结出一套AOP的流程图:

![](http://imageblog.boyn.top/201908162357_80.png)

## Spring中的AOP编程

在Spring中,最常用的是通过AspectJ进行编程,而他的核心就是@Aspect注解

我们先来定义一个Role类,作为一个简单的POJO

```java
public class Role {
    private Long id;
    private String name;
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
```

在Web编程中,我们去对一个POJO进行CRUD通常是用一个接口定义行为,并用一个实现类来实现

在此处,我们省略了Service的声明,直接给出ServiceImpl的代码

```java
@Service
public class RoleServiceImpl implements RoleService{

    @Override
    public void printRole(Role role,int sort) {
        System.out.println(role+"--"+sort);
    }

    @Override
    public void printRole(Role role) {
        System.out.println(role);
    }
}
```

注意在此处的类注解是@Service

在Spring中的AOP是方法级别的,也就是说,是以某个类的某个方法作为切点,通过拦截这个方法来织入AOP通知

在此处,切点自然地就是两个printRole方法了,第一个方法中的sort其实没有实际意义,只是为了演示后面要用的AOP代理中的表达式编写

定义好了切点,自然下一步就是定义切面了.对于我们而言,一个切面其实就是一个拦截器,在Spring中,我们只要使用@Aspect注解来注解一个类,那么IOC容器就会认为这是一个切面了.

所以我们定义一个切面的类,来对printRole方法做一个AOP的切面

```java
@Aspect
public class RoleAspect {

    @Pointcut("execution(* top.boyn.springlearn.AspectJTest.RoleServiceImpl.printRole(..))")
    public void print(){}

    @Before("print()")
    public void before(){
        System.out.println("before...");
    }

    @After("print()")
    public void after(){
        System.out.println("after...");
    }

    @AfterReturning("print()")
    public void afterReturning(){
        System.out.println("returned...");
    }
    
    @AfterThrowing("print()")
    public void afterThrowing(){
        System.out.println("throwing...");
    }

    @Around("print()")
    public void around(ProceedingJoinPoint jp) {
        System.out.println("around begin");
        try {
            jp.proceed();//jp是Spring提供的反射切点方法的参数
            //在这里运行jp.proceed等于运行print()
            //在调用proceed前,会调用before,在调用proceed后,会调用after
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        System.out.println("around end");
    }
}
```

我们先来介绍最基本的5个方法

![](http://imageblog.boyn.top/201908170008_374.png)

根据这个表,我们就可以知道各个方法执行的顺序了

在上面的代码中,除了各个方法的注解,注解里面的内容也同样重要,这个是方法的连接点定义,用于Spring判断是否需要拦截你的方法,以及拦截哪个方法

```java
@Pointcut("execution(* top.boyn.springlearn.AspectJTest.RoleServiceImpl.printRole(..))")
```

execution ---代表执行方法的时候会触发

*---代表返回任意类型的方法

top.boyn.springlearn.AspectJTest.RoleServiceImpl. ---代表类的全限定名称

printRole --被拦截方法的名称

(..) 任意参数

事实上,在AspectJ定义的指示器中,内容远比这个要丰富,根据下表,我们可以定义出具有任意逻辑表达式的连接点

![](http://imageblog.boyn.top/201908170014_576.png)

在编写好了这三个类之后,我们就可以对这个进行一下测试了,在测试之前,需要对Aspect进行配置

配置类的代码如下

```java
@Configuration
@EnableAspectJAutoProxy
@ComponentScan("top.boyn.springlearn.AspectJTest")
public class AspectConfig {

    @Bean
    public RoleAspect getRoleAspect(){
        return new RoleAspect();
    }
}
```

这个配置类的主要目的,就是通知IOC容器,这里有需要配置AOP的类,并且返回了一个Aspect的类作为Bean

在这里特别要注意的是,around方法是AOP中比较强大的功能,他可以同时实现前置与后置通知.保留了原对象方法的功能.在第一个测试中,我们不会使用around方法

```java
@Test
    public void Test(){
        Role role = new Role();
        role.setId(1L);
        role.setName("ac");
        role.setNote("123");
        roleService.printRole(role);
        roleService.printRole(role,2);
    }
```

![](http://imageblog.boyn.top/201908170019_796.png)

测试结果如上图,可以看到,在printRole方法被调用前后,都有相应的切点方法被调用

而加上了around的方法后,around方法在运行proceed前后的语句都会被运行

![](http://imageblog.boyn.top/201908170029_600.png)

