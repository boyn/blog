> 欢迎浏览我的博客 获取更多精彩文章
>
> [https://boyn.top](https://boyn.top/)

# Spring Boot静态资源解析

## 使用预定义的路径

在Web开发中,会经常用到很多静态资源,如图片,js文件等等.在SpringMvc项目中,如果我们要进行静态资源的配置,会比较麻烦,我们要在xml中定义几行文字或者在Java中设定Bean,来规定静态资源的位置和匹配的规则.那么,在Spring Boot中,我们要怎么样来进行静态资源的配置呢?

首先,Spring Boot已经为我们预先设定了4个静态文件的位置.

在org.springframework.boot.autoconfigure.web.ResourceProperties中,有着这样一个变量

```java
private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/",
			"classpath:/resources/", "classpath:/static/", "classpath:/public/" };
```

从名字上来看,这是表示初始化时加载的资源路径.

在这里有个地方值得注意,在开发时,我们看到的static文件夹实在resources下面的,但是为什么在加载时,只用在根目录下面查找呢?

这个是由于我们在最终生成包时,static包是放在了根目录下的,如图

![](http://imageblog.boyn.top/201908212054_884.png)

那么,这些资源是在哪里被加载的呢?

我们将眼光投向org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration中

我们知道  在SpringMvc中,无论是Java配置,还是xml配置,最终都是用addResourceHandlers来解析的.

同样地,在.WebMvcAutoConfiguration中也有一个继承自WebMvcAutoConfigurationAdapter的addResourceHandlers,而这个方法,是在WebMvcConfigurer接口中定义的.

在addResourceHandlers方法中,以下这段代码,正是加载预定义静态资源加载的关键

```java
customizeResourceHandlerRegistration(registry.addResourceHandler(staticPathPattern)
						.addResourceLocations(getResourceLocations(this.resourceProperties.getStaticLocations()))
						.setCachePeriod(getSeconds(cachePeriod)).setCacheControl(cacheControl));
```

## 自己定义路径

在properties文件中

```java
spring.resources.static-locations=classpath:/your-paths/
```

在yaml文件中

```yaml
spring:
	resources:
		static-locations: your-paths
```

