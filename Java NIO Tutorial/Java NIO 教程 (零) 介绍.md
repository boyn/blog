# Java NIO 教程 (零) 介绍

## 什么是NIO

从JDK1.4开始,Java NIO(New IO)开始成为Java IO API的一个可替代者,想要替代Java IO和Java Networking的API's

Java NIO (以后直接称NIO) 与传统IO的API不同,它提供了不同的接口来进行IO操作,顾名思义New IO

## NIO:Channels与Buffers

在标准IO的API中,我们通过字节流和字符流进行数据传输.而在NIO中,我们通过Channels和Buffers.数据永远从一个Channel向Buffer写入,或者从一个Buffer中取出读入到Channel

译名: Channel -- 通道,隧道   Buffer -- 缓冲区

## NIO: 非阻塞IO

NIO让IO变为非阻塞.具体来说,一个线程可以让通道向缓冲区中读入数据,而当读入数据时,线程可以做其他事情.当数据读入完毕后,线程就可以继续处理他了.写入数据同理

## NIO: 选择器

NIO有着'选择器'的概念.选择器是一个可以通过事件机制监控多个通道的对象(如连接建立,数据到达...).因此,一个线程可以管理多个通道(译者注:多路复用机制)

下一章会有更多的关于NIO是如何工作的细节 -- Java NIO 教程 (一) 