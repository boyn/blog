# Java NIO 教程 (一) 总览

说到底,其实NIO总共就是三个概念:

- 通道
- 缓冲区
- 选择器

虽然实际上,NIO中有比这些概念更多的类和组件,但是通道,缓冲区和选择器形成了最核心的API.其他的组件,比如管道与文件锁,他们仅仅是连着这三个核心组件的工具而已.因为,该教程将会聚焦于这三个组件.剩余的组件的介绍于用法会在后面教程中给出

## 通道与缓冲区

一个典型且抽象的NIO输入输出模型如下.通道有一点儿像是一个流对象.数据可以从通道写入缓冲区中,也可以从缓冲区中读入到通道中.

![](https://cdn.sinaimg.cn.52ecy.cn/large/005BYqpgly1g3oa0lpnyzj308l05u3ye.jpg)

下面是一些NIO中通道的实现类(因为通道是一个概念,而在NIO中是作为一个抽象类进行处理的,我们从概念出发到应用还会有一些距离,不过好在NIO已经为了一些常用的IO操作定义了一系列实现类)

- FileChannel(文件IO)
- DatagramChannel(UDP网络IO)
- SocketChannel(TCP网络IO)
- ServerSocketChannel(TCP网络IO)

正如你所见,NIO中的通道覆盖了UDP TCP协议的网络IO与文件IO

当然了,这些类还会有许多接口的实现,将会在以后的教程中看到他们.

同样地,下面是一些缓冲区的实现类

- ByteBuffer
- CharBuffer
- DoubleBuffer
- FloatBuffer
- LongBuffer
- ShortBuffer

这些缓冲区的实现类包含了你能通过IO传输的基本类型

NIO同时还有一个MapperByteBUffer,这是用于连接内存映射文件(memory mapped files)的.

## 选择器

选择器能够使单个线程掌管多个通道.如果你的应用有着很多通道连接,但是每个通道上传输的数据不多(或者说占用传输通道的时间少),那么多路复用机制就会十分有用.比如说,在一个聊天服务器中,登陆的用户很多,但是他们之间传输数据并不频繁(相对于机器),那么这种模型就会让线程减少,以提高性能了.其图如下

![](https://cdn.sinaimg.cn.52ecy.cn/large/005BYqpgly1g3oaas1dsxj30a00890sp.jpg)

为了使用选择器,你必须用通道向选择器进行注册,然后你可以调用select()方法.这个方法会提起阻塞直到有一个已注册通道上的事件发生.当这个方法结束并返回时,选择器的线程就可以处理这些事件了.