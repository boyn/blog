## 过滤器

##  SecurityContextPersistenceFilter

顶层过滤器,第一个作用是用于验证请求的session中是否含有特定的字段SpringSecutrityContext,如果存在,就将这个上下文放入SecurityContextHolder供Security中的其他部分使用  第二个作用是在过滤器执行完毕后,清空SecurityContextHolder的内容,因为SecurityContextHolder底层用了threadlocal,所以防止其影响并发性,要在结束时清空

## LogoutFilter

只处理注销请求.在用户发送注销请求时,销毁用户的session,然后重定向到注销成功界面

## AbstractAuthenticationProcessingFilter

进行用户的验证

## DefaultLoginPageGeneratingFilter

生成一个默认的登录页面,支持username,password和rememberme

## BasicAuthenticationFilter

进行用户的验证

## SecurityContextHolderAwareRequestFilter

用来进行请求的包装,在其中加入数据

## RememberMeAuthenticationFilter

用于记住我功能的实现,依赖cokkie实现

## AnonymousAuthenticationFilter

在用户未登录时,为用户提供一个匿名用户的实现

## ExceptionTranslationFilter

处理FilterSecurityInterceptor中抛出的异常,将请求重定向到指定页面或返回对应错误代码

## SessionManagementFilter

为了防御会话比较攻击

## FilterSecurityInterceptor

基本功能的拦截器,主要用来抛出错误的异常

## FilterChainProxy



