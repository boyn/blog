> 欢迎浏览我的博客 获取更多精彩文章
>
> [https://boyn.top](https://boyn.top/)
# Spring Boot整合Freemarker

## Spring Boot加入Freemarker依赖

在maven中加入依赖

```java
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-freemarker</artifactId>
            <version>2.1.3.RELEASE</version>
        </dependency>
```



## Spring Boot文件配置Freemarker

在FreeMarkerProperties类里面,我们可以看到常用的freemarker配置

此处选一些常用的来说

- ```java
  String DEFAULT_TEMPLATE_LOADER_PATH = "classpath:/templates/";
  //这个是选择默认的freemarker模板加载路径,默认是在/templates文件夹下面,在springboot项目中一般是resources/templates文件夹
  ```

- ```java
  String DEFAULT_PREFIX = "";
  //默认前缀,默认值为空,建议不动
  ```

- ```java
  String DEFAULT_SUFFIX = ".ftl";
  //默认后缀,即文件扩展名,为ftl,建议不动
  ```

- ```java
  String[] templateLoaderPath = new String[] { DEFAULT_TEMPLATE_LOADER_PATH };
  //默认模板加载路径集,我们可以指定多个模板加载的路径
  ```

## Freemarker常用指令

### if语句

if指令是用来判断条件是否成立的.

其语法如下

<#if contition>

<#elseif contition>

<#else>

</#if>

在这里需要注意的是,elseif中间跟java语法不一样,是没有空格的

我们实际演示一下

```java
@GetMapping("/user")
    public String getUser(Model model){
        List<User> users = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            User u = new User(i,"Boyn"+i,"password"+i, new Random().nextInt(3));
            users.add(u);
        }
        model.addAttribute("users", users);
        return "user";
    }
```

这里是一个User的Controller

```html
	<#list users as u>
        <tr>
            <td>${u.id}</td>
            <td>${u.username}</td>
            <td>${u.password}</td>
            <td>
                <#if u.gender==1>
                    男
                <#elseif u.gender==2>
                    女
                <#else>
                    其他
                </#if>
            </td>
        </tr>
    </#list>
```

这里是一段.ftl文件

接下来我们看看会有什么效果

![](http://imageblog.boyn.top/201908210934_47.png)

效果图如下

### switch case语句

这个语句和if作用相同.如果将刚刚的例子转换成switch语句表示,则如下:

```html
				<#switch u.gender>
                    <#case 1>男<#break >
                    <#case 2>女<#break >
                    <#default >其他<#break >
                </#switch>
```

### list break语句

list是一个迭代输出的语句,用于输出数据模型中的集合元素.其语法如下

<#list collection as item>

</#list>

其中和java中的循环一样,也可以在里面加入条件判断及时退出

<#list collection as item>

​	<#if contition><#break></#if>

</#list>

实际演示在if语句的代码中已经包含,不再赘述

### include语句

include语句用于加入模板文件,如头部,尾部文件等.

语法如下

<#include filename [options]>  

options设置中有encoding和parse两个选项,encoding指定编码类型,parse选择是否以freemarker解析器进行解析,如为false,则当作普通的html,默认为true

我们先创建一个header文件

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>标题</title>
</head>
<body>
<h1>个人博客 boyn.top</h1>
</body>
</html>
```

此时这两个文件在同一个目录下
![](http://imageblog.boyn.top/201908210950_56.png)

我们在user.ftl的body开始处,加入include语句 

```html
<body>
<#include "./header.ftl">
```

其效果如下:

![](http://imageblog.boyn.top/201908210951_285.png)

### noparse语句

包含在noparse标签里面的语句不会进行解析,即当作普通文本

<#noparse>

...

</#noparse>

### 

