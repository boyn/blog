> 欢迎浏览我的博客 获取更多精彩文章
>
> [https://boyn.top](https://boyn.top)

# 别再用os.path了,尝试一下pathlib吧

[TOC]

我在很久之前曾经接触过pathlib这个库,当时我认为这是一个略显笨拙,而且没有什么必要出现的面向对象版本的os.path.但是当我现在回过头来看时,pathlib真的是酷毙了!

这篇文章我会向你安利pathlib这个库,他是python的标准库之一,无需另外安装.希望你看完之后可以在以后Python开发的日子里更多得使用pathlib

## os.path太笨重了

当我们在python中操作路径和文件时,一般会用到os.path.你要用的他都会有,但是有的时候未免显得太笨重了

你是不是会这样调用os.path呢

```python
`import os.path  BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) TEMPLATES_DIR = os.path.join(BASE_DIR, 'templates') `
```

或者这样?

```python
`from os.path import abspath, dirname, join  BASE_DIR = dirname(dirname(abspath(__file__))) TEMPLATES_DIR = join(BASE_DIR, 'templates') `
```

又或者说join方法太普通了,给他换个名字?

```python
`from os.path import abspath, dirname, join as joinpath  BASE_DIR = dirname(dirname(abspath(__file__))) TEMPLATES_DIR = joinpath(BASE_DIR, 'templates')`
```

我发现这些方法用起来都有一点笨拙,不着手.我们将一个字符串传入函数中,接受返回的字符串再将这个字符串传入到另一个函数中,最终接受一个字符串.这些字符串其实都是路径,但是在python里面他们只是字符串.

这些string-in-string-out的函数在os.path中随处可见,这样用看起来有点"笨拙",因为其实这些中间字符串大多数时候我们都用不上.我们如果能把这些方法组成调用链的形式,那是不是更好呢?

在pathlib中,如果我们要做到上面os.path要做的话,可以这样做

```python
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
TEMPLATES_DIR = BASE_DIR.joinpath('templates')

```

Path类让我们能够将类中的方法和属性组成调用链.

也许你会想:Path对象和Path对象之间根本不是一个东西,他们是对象而不是字符串.不过,他们也可以和字符串进行呼唤,并且十分简单

## os模块过于臃肿

os.path只是在路径上面做文章.当你真正的想要,比如说创造文件,创造目录,你就需要os模块

os模块有着一大堆操作文件和路径的工具:`mkdir`, `getcwd`, `chmod`, `stat`, `remove`, `rename`, 和`rmdir`. 还有`chdir`, `link`, `walk`, `listdir`, `makedirs`, `renames`, `removedirs`, `unlink` 和`symlink`

os模块几乎做了所有事情.大部分与系统有关的事情都被堆积到os模块上面了.当然,os模块里面有很多有用的东西,但是我们找起来也是一个很麻烦的事情:比如说你想要找一个关于路径或者关于文件操作的函数,对于很多人来说要花一点时间在IDE上面看自动补全或者查阅官方文档.

但是pathlib专注于操作路径和文件.他能够取代os模块中很多关于文件系统的工具

假设我们要创建一个src/__pypackages_\_目录,还要将.editconfig改为src/.editconfig

我们看看在os.path里面是怎么做的

```python
import os
import os.path

os.makedirs(os.path.join('src', '__pypackages__'), exist_ok=True)
os.rename('.editorconfig', os.path.join('src', '.editorconfig'))
```

再来看看pathlib中的操作

```python
from pathlib import Path

Path('src/__pypackages__').mkdir(parents=True, exist_ok=True)
Path('.editorconfig').rename('src/.editorconfig')
```

在行数相同的情况下,pathlib显然会更加直观和清晰.并且,以可读性的角度来看,pathlib的示例把我们要操作的路径放在了比较靠前的位置,让我们读起来会更加地方便.

os模块是一个非常庞大的命名空间,很多方法变量冗杂在其中.但pathlib更加轻量级,比起os模块,对路径的操作更友好.

## pathlib让简单更简单

pathlib模块固然可以让复杂的语句变得简单,但是更好的地方在于,他可以让简单的语句变得更简单.

实例1: 从一个或多个文件中读取所有内容

```python
from pathlib import Path

file_contents = [
    path.read_text() for path in Path.cwd().rglob('*.py')
]
```

pathlib用一个列表生成式,就可以读取该目录下所有.py文件的内容

实例2:写入文件

```python
Path('.editorconfig').write_text('# config goes here')

# 或者这样(如果你需要指定打开文件的格式的话)

from pathlib import Path

path = Path('.editorconfig')
with path.open(mode='wt') as config:
    config.write('# config goes here')

```

## pathlib让你的代码更加精确

在python中,会经常使用各种字符串.但是,谁又能知道每个字符串具体表示什么呢?

```python
person = '{"name": "Trey Hunner", "location": "San Diego"}'
pycon_2019 = "2019-05-01"
home_directory = '/home/trey'
```

这个时候,面向对象的好处就出来了

```python
from datetime import date
from pathlib import Path

person = {"name": "Trey Hunner", "location": "San Diego"}
pycon_2019 = date(2019, 5, 1)
home_directory = Path('/home/trey')
```

那么我们就知道了,第一个变量是一个字典,第二个是一个日期的对象,而第三个则是一个路径的对象

pathlib会让你的代码更加精确,如果你想要表示一个路径,比起字符串,当然是路径对象要来的显然.

当然,我个人不是一个面向对象的倡议者.有时候增加类和接口,就是在现有的程序上多做一层抽象的话,会增加程序的复杂性,降低可读性.但是起码对于Pathlib来说,它是一个有用且成功的抽象.并且从Python3.6开始,内置的open函数与os模块中的很多函数以及shutil都支持Path对象了,所以如果我们要将以前的代码修改过来的话并不用花很多功夫

## pathlib中还有什么缺失?

当然,pathlib是一个非常优秀的库,但是仍然有许多我期望它有,但它缺少的功能.

第一点,它缺少对`shutil`功能的支持,所以,比如说我们想要复制一个文件,Path缺少对其的直接支持,所以我们不得不引用shutil库

```python
from pathlib import Path
from shutil import copyfile

source = Path('old_file.txt')
destination = Path('new_file.txt')
copyfile(source, destination)

```

第二,它没有更改当前目录的功能.我们需要引用chdir来改变目前的工作目录

```python
from pathlib import Path
from os import chdir

parent = Path('..')
chdir(parent)
```

更多的有,它缺少对os.walk的支持...

## 你要一直使用pathlib吗

从Python3.6开始,Path对象就已经可以替代大部分路径字符串的工作了.所以如果你使用的是Python3.6及以上的版本,没有理由不使用这个方便的库.但是如果你使用的是老版本的python,你也可以将Path对象转化为路径字符串

```python
from os import chdir
from pathlib import Path

chdir(Path('/home/trey'))  # Python 3.6+
chdir(str(Path('/home/trey')))  # 3.6之前的版本

```

当然了,如果你在使用python2.*,那么你可以在PyPi中下载安装pathlib2.

pathlib会使你的代码具有更好的易读性.现在我的很多代码都已经使用Path对象来代替路径字符串了.希望你看完这篇文章之后,你也会这样做.