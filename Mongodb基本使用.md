# Mongodb基本使用

Mongodb是一个文档型数据库,与我们熟悉的关系型数据库如mysql等不一样,它属于nosql类型的数据库.它在存储JSON格式的文本上有很大的优势.并且随着Mongodb的不断发展,他在大部分应用场景中的速度,稳定性已经不输给很多传统数据库.

## Mongodb的安装

在网络上已经有很多安装Mongodb的教程了.我在这里推荐两个:

- 菜鸟教程:[Windows安装Mongodb](https://www.runoob.com/mongodb/mongodb-window-install.html)
- 官方安装指南:https://www.runoob.com/mongodb/mongodb-window-install.html

如果嫌麻烦,我们可以使用云数据库,在Mongodb的官网推荐了一个可以免费使用的[云Mongodb数据库](https://docs.mongodb.com/manual/tutorial/atlas-free-tier-setup/)

## Mongodb基本命令

### 数据库管理

#### 切换数据库

在Mongodb中,数据库管理集合,集合包含若干文档.集合可以看作传统数据库中的表,文档就是每一行数据.

我们如果要选择使用其中的一个数据库,可以用以下语句

```sql
use MyDB
```

#### 创建数据库

Mongodb在我们没有数据库时,如果对其插入了一条新的数据,则会自动创建这样的一个数据库

```
use MyDB //假设这时候MyDB不存在

db.MyCollections.insertOne({x:1})
```

这个时候,insertOne的操作会自动创建名为MyDB的数据库,名为MyCollections的集合,并在其中插入一个文档

### 集合操作

#### 创建一个集合

```
db.myNewCollection2.insertOne( { x: 1 } )
db.myNewCollection3.createIndex( { y: 1 } )
```

在上面的两条Collection不存在的情况下,执行语句,这些集合都会被创建

如果你想要在创建集合时,规定集合的一些元参数,如最大值,或者插入时的验证规则等等,你可以用```db.createCollection()```方法来进行创建.对其创造时候详细的参数设置,可参见[这个链接](https://docs.mongodb.com/manual/reference/command/collMod/#dbcmd.collMod)

#### 验证集合

通常来说,一个集合不对其中的字段作要求.但是,你可以强制指定要遵循的[验证规则](https://docs.mongodb.com/manual/core/schema-validation/)

### ⭐CRUD⭐

#### 插入操作

通常会用两个方法来进行文档插入

db.collection.insertOne()

db.collection.insertMany()

参数是JSON对象与JSON对象数组

MongoDB中没有事务管理机制,但是它确保了对同一个文档来说,所有的写入操作都是原子的,即使是更改单个文档中的多个嵌入文档(即嵌套JSON对象)

下面这个语句插入了一段简单的json文档进入inventory集合中

```sql
db.inventory.insertOne(
   { item: "canvas", qty: 100, tags: ["cotton"], size: { h: 28, w: 35.5, uom: "cm" } }
)
```

insertOne会返回一个包含了新插入文档的_id字段的文档

```json
{
    "acknowledged": true,
    "insertedId": ObjectId("5d762d93b400000011006ff2")
}
```

下面这段语句插入了包含json数组的文档

```sql
db.inventory.insertMany([
   { item: "journal", qty: 25, tags: ["blank", "red"], size: { h: 14, w: 21, uom: "cm" } },
   { item: "mat", qty: 85, tags: ["gray"], size: { h: 27.9, w: 35.5, uom: "cm" } },
   { item: "mousepad", qty: 25, tags: ["gel", "blue"], size: { h: 19, w: 22.85, uom: "cm" } }
])
```

则会返回如下的字段

```json
{
    "acknowledged": true,
    "insertedIds": [
        ObjectId("5d7635a8b400000011006ff3"),
        ObjectId("5d7635a8b400000011006ff4"),
        ObjectId("5d7635a8b400000011006ff5")
    ]
}
```

关于_id字段,在Mongodb中,每一个文档都会有一个\_id字段作为主键,插入时可以不需要指定这个字段,mongodb会自动生成.

参考:

[insertOne](https://docs.mongodb.com/manual/reference/method/db.collection.insertOne/#db.collection.insertOne)

[insertMany](https://docs.mongodb.com/manual/reference/method/db.collection.insertMany/#db.collection.insertMany)

#### 读取操作

我们可以使用以下命令来从集合中检索文档

```sql
db.collection.find()
```

检索文档的操作比较复杂,所以会在另一篇文章中作详细说明

参考

[find()](https://docs.mongodb.com/manual/reference/method/db.collection.find/#db.collection.find)

#### 更新操作

更新操作比较简单,其选择方式与读取操作类似

- [`db.collection.updateOne()`](https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/#db.collection.updateOne) 
- [`db.collection.updateMany()`](https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/#db.collection.updateMany) 
- [`db.collection.replaceOne()`](https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/#db.collection.replaceOne)

#### 删除操作

mongodb可以用以下两个方法来删除元素

```sql
db.collection.deleteOne()
db.collection.deleteMany()
```

