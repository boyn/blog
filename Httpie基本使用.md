> 欢迎浏览我的博客 获取更多精彩文章
>
> https://boyn.top
>

# HTTPie基本使用

## 1.什么是HTTPie

HTTPie是一个命令行版本的HTTP客户端.它旨在尽可能地让我们用命令行访问HTTP服务的体验更加友好.它提供了一个http命令,我们可以通过这个命令,只需要使用简单和自然的语法,就可以得到http的响应文件了.

它多用于测试,debug和与服务器传输信息

## 2.主要用法

在这里,我用Node.JS搭建了一个简单的服务器,源码在最后给出

### 2.1 基本语法

http [flags] [METHOD] URL [ITEM [ITEM]]

#### 例子

简单的包含请求方法,请求头,和json格式的请求

```shell
http GET localhost:8081/httpie name==a age==12
#等价于 GET /httpid?name=a&age=12 HTTP/1.1
```

返回值

```shell
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 23
Content-Type: text/html; charset=utf-8
Date: Wed, 31 Jul 2019 14:54:10 GMT
ETag: W/"17-dBI1xrwNGBYYt4uC14H/rFMHuh4"
X-Powered-By: Express

{
    "age": "12", 
    "name": "a"
}

```

