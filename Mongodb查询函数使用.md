> 欢迎浏览我的博客 获取更多精彩文章
>
> [https://boyn.top](https://boyn.top)

# Mongodb查询函数使用

在Mongodb中,查询操作并不复杂,但是却十分常用.

下面我们会通过一个例子来说明查询函数的使用方法.

首先,我们要先在Mongo Shell里面插入对应的数据

```sql
db.inventory.insertMany([
   { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "A" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" }
]);
```

## 查询所有数据

如果要查询表中所有的数据,可以使用以下语句

```sql
db.inventory.find( {} )
```

等价于以下SQL

```sql
SELETE * FROM inventory
```

## 查询相等数据

如果要查询指定的数据,我们要构造一个键值对列表`{<fiele1>:<value1>}`来作为过滤器

```sql
db.inventory.find({status:"D"})
```

等价于以下语句

```sql
SELETE * FROM inventory where status="D"
```

查询结果如下

```json
// 1
{
    "_id": ObjectId("5d76f85cb400000011006ff8"),
    "item": "paper",
    "qty": 100,
    "size": {
        "h": 8.5,
        "w": 11,
        "uom": "in"
    },
    "status": "D"
}

// 2
{
    "_id": ObjectId("5d76f85cb400000011006ff9"),
    "item": "planner",
    "qty": 75,
    "size": {
        "h": 22.85,
        "w": 30,
        "uom": "cm"
    },
    "status": "D"
}
```

## 查询给定条件的数据

可以通过构造以下键值对来进行特定条件的查询

```json
{ <field1>: { <operator1>: <value1> }, ... }
```

比如说,我们要查询status为A或者D的数据,可以这样

```sql
db.inventory.find( { status: { $in: [ "A", "D" ] } } )
```

结果为

```json
// 1
{
    "_id": ObjectId("5d76f85cb400000011006ff6"),
    "item": "journal",
    "qty": 25,
    "size": {
        "h": 14,
        "w": 21,
        "uom": "cm"
    },
    "status": "A"
}

// 2
{
    "_id": ObjectId("5d76f85cb400000011006ff7"),
    "item": "notebook",
    "qty": 50,
    "size": {
        "h": 8.5,
        "w": 11,
        "uom": "in"
    },
    "status": "A"
}

// 3
{
    "_id": ObjectId("5d76f85cb400000011006ff8"),
    "item": "paper",
    "qty": 100,
    "size": {
        "h": 8.5,
        "w": 11,
        "uom": "in"
    },
    "status": "D"
}

// 4
{
    "_id": ObjectId("5d76f85cb400000011006ff9"),
    "item": "planner",
    "qty": 75,
    "size": {
        "h": 22.85,
        "w": 30,
        "uom": "cm"
    },
    "status": "D"
}

// 5
{
    "_id": ObjectId("5d76f85cb400000011006ffa"),
    "item": "postcard",
    "qty": 45,
    "size": {
        "h": 10,
        "w": 15.25,
        "uom": "cm"
    },
    "status": "A"
}

```

关于操作符,官网网站给出了支持的[条件列表](https://docs.mongodb.com/manual/reference/operator/query/#query-selectors)

## 使用AND指定条件

在find()函数中,可以给定多个键值对的对象作为查询条件,这些键值对对象在逻辑上是用**与**连接在一起的.只有当文档满足所有给出的条件时,才会被查询到

比如说,我们想要查找一个status为A并且qty小于30的文档

```sql
db.inventory.find( { status: "A", qty: { $lt: 30 } } )
```

结果如下

```json
// 1
{
    "_id": ObjectId("5d76f85cb400000011006ff6"),
    "item": "journal",
    "qty": 25,
    "size": {
        "h": 14,
        "w": 21,
        "uom": "cm"
    },
    "status": "A"
}
```

## 使用OR指定条件

在有的时候,我们想要的是几个条件任意满足其一即可,那么我们就可以用OR操作符来指定条件.

比如说,我们想要查找一个status为A或者qty小于30的文档

```sql
db.inventory.find( { $or: [ { status: "A" }, { qty: { $lt: 30 } } ] } )
```

需要注意的是,在这里,两个查询条件是作为$or列表的两个元素分开的,而不是像上一节一样在一个对象中.

如果我们误将条件写成了这样

```sql
db.inventory.find( { $or: [ { status: "A" , qty: { $lt: 30 } } ] } )
```

那么,$or列表只有一个元素,其实就变成了AND条件

执行正确语句时,结果如下

```json
// 1
{
    "_id": ObjectId("5d76f85cb400000011006ff6"),
    "item": "journal",
    "qty": 25,
    "size": {
        "h": 14,
        "w": 21,
        "uom": "cm"
    },
    "status": "A"
}

// 2
{
    "_id": ObjectId("5d76f85cb400000011006ff7"),
    "item": "notebook",
    "qty": 50,
    "size": {
        "h": 8.5,
        "w": 11,
        "uom": "in"
    },
    "status": "A"
}

// 3
{
    "_id": ObjectId("5d76f85cb400000011006ffa"),
    "item": "postcard",
    "qty": 45,
    "size": {
        "h": 10,
        "w": 15.25,
        "uom": "cm"
    },
    "status": "A"
}

```

## 同时指定OR与AND条件

通过上面,我们可以将or,与and条件结合起来,只需要记住or和and不同的调用顺序就可以随心所欲了.

我们来看下面这个例子

```sql
db.inventory.find( {
     status: "A",
     $or: [ { qty: { $lt: 30 } }, { item: /^p/ } ]
} )
```

他的条件为 status=A and (qty<30 or item为p开头)

这里对item的匹配为正则表达式,结果如下

```json
// 1
{
    "_id": ObjectId("5d76f85cb400000011006ff6"),
    "item": "journal",
    "qty": 25,
    "size": {
        "h": 14,
        "w": 21,
        "uom": "cm"
    },
    "status": "A"
}

// 2
{
    "_id": ObjectId("5d76f85cb400000011006ffa"),
    "item": "postcard",
    "qty": 45,
    "size": {
        "h": 10,
        "w": 15.25,
        "uom": "cm"
    },
    "status": "A"
}

```



关于对查询的更多使用,可以参见[官方文档](https://docs.mongodb.com/manual/tutorial/query-documents/).

