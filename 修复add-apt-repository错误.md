# Deepin 15.10系统修复add-apt-repository故障


> 原文地址:
>
> https://itsfoss.com/add-apt-repository-command-not-found/
> 
> https://unix.stackexchange.com/questions/520715/deepin-15-10-error-could-not-find-a-distribution-template-for-deepin-stable

在使用debian系的linux发行版时,常常会用到ppa(个人发行包)来安装软件,如果我们想要添加一个新的ppa库,我们就需要运行以下命令

```shell
sudo add-apt-repository ppa:some/ppa
```

但是,我们通常会见到以下的错误

> **sudo: add-apt-repository: command not found**

## 修复add-apt-repository

让我们来看看如何修复这个错误

这个问题非常简单,就是add-apt-repository这个软件并没有安装,我们只需运行以下命令,安装软件配置管理器,就可以了

```shell
sudo apt-get install software-properties-common
sudo apt update
```

然后就可以按照正常步骤来添加ppa源了

## Deepin 15.10修复该功能

我们在运行了上面的步骤后,再次添加ppa源,会发现爆出以下错误

> Error: could not find a distribution template for Deepin/stable

这个是由于从15.10版本开始,虽然用的内核是stable的,但是在分发版本的配置中,将其设置为了unstable版本,所以我们需要对其进行一些更改

```shell
sudo vim /usr/share/python-apt/templates/Deepin.info
```

将

> Suite: unstable

改为

> Suite: stable